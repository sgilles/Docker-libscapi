FROM ubuntu:latest
LABEL maintainer Sébastien Gilles "sebastien.gilles@inria.fr"

RUN (apt-get update && apt-get upgrade -y -q && apt-get dist-upgrade -y -q)

# ---------------------------------------
# Install recent version of gcc and g++
# [ABORTED]: one of the embedded library (json) doesn't compile with gcc 7.
# ---------------------------------------

# To provide add-apt-repository 
# RUN apt-get install --no-install-recommends -y software-properties-common
#
# ENV GCC_VERSION 7
#
# RUN add-apt-repository ppa:ubuntu-toolchain-r/test && apt-get update \
#     && apt-get install -y gcc-$GCC_VERSION g++-$GCC_VERSION


# ---------------------------------------
# Install all prerequisites.
# Boost is not one of them, contrary to the statement on their website: they actually embed a version of it in
# their library.
# ---------------------------------------

RUN apt-get install --no-install-recommends -y  git build-essential libssl-ocaml-dev libssl-dev libgmp3-dev wget ca-certificates cmake gcc g++ &&\
    rm -rf /var/cache/apk/*
    

# Just now set gcc to the latest version (if this command was run before the apt-get install it would have beem reversed...)
# RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-$GCC_VERSION 100 \
#     --slave /usr/bin/g++ g++ /usr/bin/g++-$GCC_VERSION

# ---------------------------------------
# Get libscapi and compile it.
# ---------------------------------------

RUN git clone https://github.com/cryptobiu/libscapi.git /home/libscapi
              
WORKDIR /home/libscapi

ENV HOME /home

RUN make

RUN cd samples && make
